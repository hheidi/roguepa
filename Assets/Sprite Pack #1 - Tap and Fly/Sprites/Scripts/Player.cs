﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player : MonoBehaviour {
	public static float playerX;
	public static float playerY;

	public Text healthText;
	private int playerHealth = 50;
	private int healthPerCoin = 5;
	private bool jump = false;

	public float mSpeed;

	void Start ()
	{
		mSpeed = 7f;
		healthText.text = "Health: " + playerHealth;
	}

	void Update ()
	{
		transform.Translate (mSpeed * Input.GetAxis ("Horizontal") * Time.deltaTime, 0f, mSpeed * Input.GetAxis("Vertical") * Time.deltaTime);
		playerX = transform.position.x;
		playerY = transform.position.y;
		if (Input.GetKey (KeyCode.Space)) {
			JumpPlayer();
		}
		}

	private void GoUp() {
		transform.Translate (0, 0.1f, 0);
	}

	private void JumpFalse() {
		jump = false;
	}

	private void JumpPlayer() {
		if (!jump) {
			jump = true;
			Invoke ("GoUp", 0.02f);
			Invoke ("GoUp", 0.04f);
			Invoke ("GoUp", 0.06f);
			Invoke ("GoUp", 0.08f);
			Invoke ("GoUp", 0.1f);
			Invoke ("GoUp", 0.12f);
		    Invoke ("JumpFalse", 0.13f);
		}
	}

	private void OnTriggerEnter2D(Collider2D objectPlayerCollidedWith)
	{
		playerHealth += healthPerCoin;
		healthText.text = "+" + healthPerCoin + "Health\n" + "Health: " + playerHealth;
		objectPlayerCollidedWith.gameObject.SetActive (false);
}
}