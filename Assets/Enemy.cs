﻿using UnityEngine;
using System.Collections;

/*
public class Enemy : MonoBehaviour {

	void Update() {
		if(transform.position.x < playerX) {
			transform.Translate(-0.5f, 0, 0);
	}
		else {
			transform.Translate(0.5f, 0, 0);
		}

}
*/
using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

	public Vector3 pointB;

	IEnumerable Start()
	{var pointA = transform.position;
		while (true){
			yield return StartCoroutine(MoveObject(transform, pointA, pointB, 3.0f));
			yield return StartCoroutine(MoveObject(transform, pointA, pointB, 3.0f));
		}
	}

	IEnumerator MoveObject(Transform thisTransform, Vector3 startPos, Vector3 endPos, float time)
	{
		var i = 0.0f;
		var rate = 1.0f / time;
		while (i < 1.0f) {
			i += Time.deltaTime * rate;
			thisTransform.position = Vector3.Lerp (startPos, endPos, i);
			yield return null;
		}
	}

}