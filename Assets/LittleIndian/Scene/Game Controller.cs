﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class GameController : MonoBehaviour {

	private GameObject startScreen;
	private Text startText;
	private GameObject startImage;
	private int secondsUntilLevelStart =2;

	void Start () {
		startScreen = GameObject.Find ("Start");
		InitializeGame ();
	}

	private void InitializeGame()
	{
		startImage = GameObject.Find ("Start");
		startText = GameObject.Find ("Start").GetComponent<Text> ();
		startText.text = "Start";
		startImage.SetActive (true);
	    Invoke ("DisableLevelImage", secondsUntilLevelStart);
	}
	private void DisableLevelImage()
	{
		startImage.SetActive (false);
	}
}